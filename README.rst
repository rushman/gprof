Greenlet profiler (gprof)
=========================

Measures `CPU time <http://en.wikipedia.org/wiki/CPU_time>`_ (instead of `wall time <http://en.wikipedia.org/wiki/Wall-clock_time>`_) and designed specially for greenlets. Can help you to find CPU heavy parts of your code and improve responsiveness of your gevent-based application.


Example usage:

.. code:: python

    from gprof import GProfiler

    p = GProfiler()
    p.start()
    myapplication.run()
    p.stop()
    p.save('profile')

